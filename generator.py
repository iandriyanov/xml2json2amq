#-*- coding: utf-8 -*-

############################################
#
# File Name : generator.py
#
# Purpose : Запуск и работа из python3.
#
# Creation Date : 09-07-2014
#
# Last Modified : Ср. 09 июля 2014 12:00:52
#
# Created By : iandriyanov
#
############################################


import xmltodict
import json
import sys
import urllib
import stomp
import random
import exception


DEBUG = False

"""
Берет в первом аргументе запуска скрипта, ссылку на сайт.xml
или локальный файл.xml. Прогоняя по функциям, сравнивает что
конкретно передали в аргументе ссылку или файл, в зависимости 
от полученного результата происходит коректная обработка xml
в словарь(dict).

Далее из словаря генерируется простой json. Без валидации.
И полученая строка отправляется в очередь на сервер Брокера сообщения
в данном случае ActiveMQ(apache)
"""

def xmlurlparse(src):
    """
    This function takes as argument the link and it tries to parse it,
    to obtain from xml dictionary.
    """
    geturl = urllib.request.urlopen(src)
    readurl = geturl.read().decode()
    encode_url = urllib.parse.unquote_plus(readurl, encoding="cp1251")
    out = xmltodict.parse(encode_url)
    return out

def xmlfileparse(src):
    """
    This function takes as an argument a local file or 
    a link to a file in the system and attempts to parse it,
    to obtain from xml dictionary.
    """
    file = open(src)
    read = file.read()
    out = xmltodict.parse(read)
    return out

def xmltojson(string):
    """
    This feature makes the conversion dict in json
    """
    fstr = urllib.parse.unquote_plus(json.dumps(string), encoding="cp1251")
    return fstr

def identificator(objct):
    """
    A function that determines what she is given to the input file or link
    """
    identificator
    if "http" in objct:
        ident_obj = "url"
    else:
        ident_obj = "file"
    return ident_obj

class MyListener(object):
    def on_error(self, headers, message):
        print('Received an error %s' % message)

    def on_message(self, headers, message):
        print('received a message %s' % message)

_host = "localhost"
_port = 61613

if __name__ == "__main__":
    try:
        getfile_objct = identificator(sys.argv[1])
        if getfile_objct == "url":
            src = sys.argv[1]
            result = xmltojson(xmlurlparse(src))
        elif getfile_objct == "file":
            src = sys.argv[1]
            result = xmltojson(xmlfileparse(src))
        
        if DEBUG:
            print(result)

        if len(sys.argv[2]) != 0:
            _host = sys.argv[2]

        if len(sys.argv[3]) != 0:
            _port = int(sys.argv[3])

        conn = stomp.Connection([(_host, _port)])
        conn.set_listener('', MyListener())

        conn.start()
        conn.connect()
        #conn.subscribe(destination="/queue/ttt1", ack='auto', id=random.randint(1, 99))
        conn.send(destination="/queue/ttt1", header="ttt1-head-prop", body=result, message="ttt1-message-prop", id="work-comp", ack="client")
        conn.disconnect()
        conn.stop()
        
        print("A message is sent tio the queue")
 
    except IndexError:
        print("""
        Нужен первый аргумент, в котором следует указать:
        - имя файла на диске или 
        - ссылку в интернет. 
        
        Пример:
        File#! python generator.py filename[.xml]
        URL#!  python generator.py \"http://informer.gismeteo.ru/xml/27612_1.xml\"
        """)
    except exception.ConnectFailedException:
        print("Проверьте настройки подключения к message broker (*MQ)")
    
